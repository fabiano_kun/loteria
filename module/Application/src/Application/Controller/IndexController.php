<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Loteria\Adapter\LotofacilAdapter;
use Loteria\Apuracao\Lotofacil\Lotofacil;
class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $adLotofacil=$this->getServiceLocator()->get('Loteria\Adapter\Lotofacil');
        $adLotofacil instanceof LotofacilAdapter;
        $adLotofacil->setConcurso('1016');
        try {
            $result=$adLotofacil->getSorteio();
        } catch (\Loteria\Exception\ConcursoNotFoundException $exc) {
            echo $exc->getMessage();
            exit;
        }

        
        $confLotofacil=$this->getServiceLocator()->get('Loteria\Conferir\Lotofacil');
        $confLotofacil instanceof Lotofacil;
        $confLotofacil->adicionaNumerosApostados([3,4,7,9,10,11,15,16,17,19,20,21,23,24,25]);
        $confLotofacil->adicionaNumerosSorteados($result->getNumerosSorteados());
        
        
    }
}
