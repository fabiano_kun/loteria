<?php
namespace Loteria\Apuracao\Megasena;
use Loteria\Apuracao\iLoteriaApuracao;
use Loteria\Apuracao\Loteria;
/**
 * @author Fabiano
 */
class MegaSena extends Loteria implements iLoteriaApuracao {
    function __construct() {
        $this->qntNumerosMaximoAposta=9;
        $this->qntNumerosMinimoAposta=6;
        $this->qntMinimaAcertos=4;
        $this->qntMaximaAcertos=6;
        $this->maiorNumeroSorteio=60;
        $this->menorNumeroSorteio=1;
    }
    
}