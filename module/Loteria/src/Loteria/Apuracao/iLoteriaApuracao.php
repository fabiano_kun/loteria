<?php
namespace Loteria\Apuracao;

/**
 * @author Fabiano
 */
interface iLoteriaApuracao {
    public function adicionaNumerosApostados($num);
    public function adicionaNumerosSorteados($num);
}
