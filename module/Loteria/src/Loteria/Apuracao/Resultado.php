<?php
namespace Loteria\Apuracao;

/**
 * @author Fabiano
 */
class Resultado {
    const GANHOU='You win';
    CONST PERDEU='Ganhou experiência';

    public $numerosApostados;
    public $numerosSorteados;
    public $quantidadeAcertos;
    public $numerosAcertados;
    public $status;
    public $mensagem;
    public $concurso;
}
