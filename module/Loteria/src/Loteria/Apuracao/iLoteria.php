<?php
namespace Loteria\Apuracao;

/**
 * @author Fabiano
 */
interface iLoteria {
    public function adicionaNumerosApostados($num);
    public function adicionaNumerosSorteados($num);
}
