<?php
namespace Loteria\Apuracao\Lotofacil;
use Loteria\Apuracao\iLoteriaApuracao;
use Loteria\Apuracao\Loteria;
/**
 * @author Fabiano
 */
class Lotofacil extends Loteria implements iLoteriaApuracao{
   public function __construct() {
        $this->qntNumerosMaximoAposta=18;
        $this->qntNumerosMinimoAposta=15;
        $this->qntMinimaAcertos=11;
        $this->qntMaximaAcertos=15;
        $this->maiorNumeroSorteio=25;
        $this->menorNumeroSorteio=1;
    }
}
