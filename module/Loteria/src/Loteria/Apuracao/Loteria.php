<?php
namespace Loteria\Apuracao;

use Loteria\Apuracao\Resultado;
/**
 * @author Fabiano
 */
abstract class Loteria {
    /**
     * Numeros sorteados na loteria
     * Exemplo:
     * array(
     *      array(1,2,3,4,5,6),
     *      array(7,8,9,10,11,12)
     * )      * 
     * @var int[] 
     */
    protected $numerosSorteados;
    /**
     * Numeros apostados na loteria
     * Exemplo:
     * array(
     *      array(1,2,3,4,5,6),
     *      array(7,8,9,10,11,12)
     * )       
     * @var int[] 
     */
    protected $numerosApostados;
    /**
     * quantidade de numeros minimos que devem ser apostados ex: Megasena o minimo é 6
     * @var int 
     */
    protected $qntNumerosMinimoAposta;
    /**
     * quantidade de numeros máxima que devem ser apostados ex: Megasena o máximo é 8
     * @var int 
     */
    protected $qntNumerosMaximoAposta;
    
    protected $qntMinimaAcertos;
    
    protected $qntMaximaAcertos;
    
    protected $menorNumeroSorteio;
    
    protected $maiorNumeroSorteio;
    /**
    * Informe os numeros a serem apostados
    * @param int[] $num
    * @throws Exception
    */
    public function adicionaNumerosApostados($num){
        if(is_array($num)){
            array_walk($num, 'self::is_AllowNumber');
            
            if(count($num)<$this->qntNumerosMinimoAposta){
                throw new \Exception("Quantidade de números informados é inferior ao mínimo para aposta. "
                        . "Qnt informada: ".count($num)." "
                        . "Qnt mínima: ".$this->qntNumerosMinimoAposta." "
                        . "Números informados: ".implode(',',$num));
            }elseif(count($num)>$this->qntNumerosMaximoAposta){
                throw new \Exception("Quantidade de números informados é superior ao máximo para aposta. "
                        . "Qnt informada: ".count($num)." "
                        . "Qnt máxima: ".$this->qntNumerosMaximoAposta." "
                        . "Números informados: ".implode(',',$num));
            }
            sort($num);
            $this->numerosApostados[]=$num;  
        }else{
            throw new \Exception("Esperado um array de numeros");
        }
    }
    
    /**
    * Informe os numeros sorteados
    * @param int[] $num array de numeros inteiros
    * @throws Exception
    */
    public function adicionaNumerosSorteados($num){
        if(is_array($num)){
            array_walk($num, "self::is_AllowNumber");
            if(count($num)<$this->qntMaximaAcertos){
                throw new \Exception("Quantidade de números informados é menor que a quatidade esperada para aposta."
                        . "Qnt informada: ".count($num)." "
                        . "Números informados: ".implode(',',$num));
            }
            sort($num);
            $this->numerosSorteados[]=$num;
        }else{
            throw new \Exception("Esperado um array de numeros");
        }
    }
    
    public function clearNumerosSorteados(){
        $this->numerosSorteados=array();
    } 
    
    public function clearNumerosApostados(){
        $this->numerosApostados=array();
    } 
    
    public function apurarResultado(){
        $resultado= array();
        foreach ($this->numerosApostados as $jogosaposta) {
            $idx= count($resultado);
            foreach ($this->numerosSorteados as $jogosoteado) {
                $res= new Resultado();
                $res->numerosApostados=$jogosaposta;
                $res->numerosSorteados=$jogosoteado;
                $numIguais=array_intersect($jogosoteado, $jogosaposta);
                $res->quantidadeAcertos=count($numIguais);
                $res->numerosAcertados=array();
                foreach ($numIguais as $value) {                    
                    $res->numerosAcertados[]=$value;
                }
                if($res->quantidadeAcertos>=$this->qntMinimaAcertos && $res->quantidadeAcertos<$this->qntMaximaAcertos){
                    $res->mensagem="Ganhou com ".$res->quantidadeAcertos." acertos.";
                    $res->status=  Resultado::GANHOU;
                }elseif($res->quantidadeAcertos===$this->qntMaximaAcertos){
                    $res->mensagem="Ganhou acertando a quantidade máxima, com ".$res->quantidadeAcertos." acertos.";                    
                    $res->status=  Resultado::GANHOU;
                }else{
                    $res->mensagem="Mais sorte na próxima.";                    
                    $res->status=  Resultado::PERDEU;
                }
                $resultado[$idx][]=$res;
            }
        } 
        return $resultado;
    }
    
    protected function is_AllowNumber(&$valor,$chave){
        $valor=is_numeric($valor)?intval($valor):$valor;
        switch ($valor) {
            case $valor>$this->maiorNumeroSorteio:
                throw new \Exception('O maior número sorteado é '.$this->maiorNumeroSorteio.' Número informado '.$valor.'!');
            case $valor<$this->menorNumeroSorteio:
                throw new \Exception('O menor número sorteado é '.$this->menorNumeroSorteio.' Número informado '.$valor.'!');
            case !is_int($valor):
                throw new \Exception('Esperado somente numeros!');
        }
    }
}
