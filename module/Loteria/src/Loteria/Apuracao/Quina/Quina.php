<?php
namespace Loteria\Apuracao\Quina;
use Loteria\Apuracao\iLoteriaApuracao;
use Loteria\Apuracao\Loteria;
/**
 * @author Fabiano
 */
class Quina extends Loteria implements iLoteriaApuracao {
    function __construct() {
        $this->qntNumerosMaximoAposta=7;
        $this->qntNumerosMinimoAposta=5;
        $this->qntMinimaAcertos=3;
        $this->qntMaximaAcertos=5;
        $this->maiorNumeroSorteio=80;
        $this->menorNumeroSorteio=1;
    }
    
}
