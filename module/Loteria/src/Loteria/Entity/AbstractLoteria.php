<?php
namespace Loteria\Entity;

/**
 * @author Fabiano
 */
abstract class AbstractLoteria {
    protected $concurso;
    /**
     * @var \DateTime
     */
    protected $dataSorteio;
    protected $numerosSorteados;
    function __construct() {
        $this->numerosSorteados= array();
    }
    public function getConcurso() {
        return $this->concurso;
    }
    /**
     * @return \DateTime
     */
    public function getDataSorteio() {
        return $this->dataSorteio;
    }

    public function getNumerosSorteados() {
        return $this->numerosSorteados;
    }

    public function setConcurso($concurso) {
        $this->concurso = $concurso;
    }

    public function setDataSorteio(\DateTime $dataSorteio) {
        $this->dataSorteio = $dataSorteio;
    }

    public function setNumerosSorteados(array $numerosSorteados) {
        $this->numerosSorteados = $numerosSorteados;
    }

}