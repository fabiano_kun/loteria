<?php
namespace Loteria\Adapter\Lotofacil;
use Loteria\Adapter\Adapter;

class Lotofacil extends Adapter{
    protected $urlBase='http://www1.caixa.gov.br/loterias/loterias/lotofacil/lotofacil_pesquisa_new.asp';
    protected $concurso;
    
    public function getConcurso() {
        return $this->concurso;
    }

    public function setConcurso($concurso) {
        $this->concurso = $concurso;
    }

    public function getSorteio() {
        $url=$this->urlBase.'?ocao=concurso&submeteu=sim&txtconcurso='.$this->getConcurso();
        $result = $this->exec($url);
        if ($result == '') {
            return false;
        }
        
        
        
    }

}