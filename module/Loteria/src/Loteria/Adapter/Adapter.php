<?php
namespace Loteria\Adapter;

abstract class Adapter{
    function exec($url){
        $html_parser = FALSE;		
        if(!function_exists('curl_init')){
            throw new \Exception('Não tem curl Instalado');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__.'/cookie.txt');
	    curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__.'/cookie.txt');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $ch_result = curl_exec($ch);
        if (!$ch_result) {
            echo '<pre>';
            echo __DIR__.'/cookie.txt<br>';
            $info = curl_getinfo($ch);
            curl_close($ch);
            die(var_export($info));
        }
        curl_close($ch);

        if ($ch_result != '') {
            $html_parser = $ch_result;
        }

        return $html_parser;
    }
    abstract function getSorteio();
}