<?php

namespace Loteria\Adapter;
use Loteria\Adapter\Adapter;
/**
 * @author Fabiano
 */
class QuinaAdapter extends Adapter{
    protected $urlBase='http://www1.caixa.gov.br/loterias/loterias/quina/quina_pesquisa_new.asp';
    protected $concurso;
    
    public function getConcurso() {
        return $this->concurso;
    }

    public function setConcurso($concurso) {
        $this->concurso = $concurso;
    }

    public function getSorteio() {
        if($this->getConcurso()){
            $url=$this->urlBase.'?opcao=concurso&submeteu=sim&txtConcurso='.$this->getConcurso();
        }else{
            $url=$this->urlBase;
        }
        $result = $this->exec($url);
       
        if ($result == '') {
            throw new \Loteria\Exception\ConcursoNotFoundException('QuinaAdapter: Nenhum resultado encontrado.');
        }
        
        $result = explode('|', $result);
        if(count($result)<5){
            throw new \Loteria\Exception\ConcursoNotFoundException('QuinaAdapter: Concurso '.$result[0].' não encontrado.');
        }
        $ms = new \Loteria\Entity\Quina();
        $ms->setConcurso($result[0]);
        $data=  explode('/',$result[16]);
        $dataSorteio=  mktime(0, 0, 0, $data[1], $data[0], $data[2]);
        $ms->setDataSorteio(new \DateTime(date('Y-m-d',$dataSorteio)));
        $numeros= array_slice($result, 14, 1);
        $tnum=preg_replace("/\<ul\>/", '||',preg_replace("/\<li\>/", ';',$numeros[0]));
        $tnum=preg_replace("/\<\/ul\>/", '||',preg_replace("/\<\/li\>/", ';',$tnum));
        $t=(explode('||;',$tnum));
        $t=end($t);
        $arr=explode(';',$t);
        $resultado= array();
        foreach ($arr as $value) {
            if($value!=='' and is_numeric($value)){
                $resultado[]=$value;
            }
        }
        $ms->setNumerosSorteados($resultado);
        return $ms;
    }
    
}
