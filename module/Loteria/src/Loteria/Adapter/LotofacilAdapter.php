<?php
namespace Loteria\Adapter;
use Loteria\Adapter\Adapter;

class LotofacilAdapter extends Adapter{
    protected $urlBase='http://www1.caixa.gov.br/loterias/loterias/lotofacil/lotofacil_pesquisa_new.asp';
    protected $concurso;
    
    public function getConcurso() {
        return $this->concurso;
    }

    public function setConcurso($concurso) {
        $this->concurso = $concurso;
    }
    /**
     * @return \Loteria\Entity\Lotofacil
     * @throws \Loteria\Exception\ConcursoNotFoundException
     */
    public function getSorteio() {
        if($this->getConcurso()){
            $url=$this->urlBase.'?opcao=concurso&submeteu=sim&txtConcurso='.$this->getConcurso();
        }else{
            $url=$this->urlBase;
        }
        $result = $this->exec($url);
       
        if ($result == '') {
            throw new \Loteria\Exception\ConcursoNotFoundException('LotofacilAdapter: Nenhum resultado encontrado.'.$url);
        }
        
        $result = explode('|', $result);
        if(count($result)<5){
            throw new \Loteria\Exception\ConcursoNotFoundException('LotofacilAdapter: Concurso '.$result[0].' não encontrado.');
        }
        $lotofacil = new \Loteria\Entity\Lotofacil();
        $lotofacil->setConcurso($result[0]);
        $data=  explode('/',$result[34]);
        $dataSorteio=  mktime(0, 0, 0, $data[1], $data[0], $data[2]);
        $lotofacil->setDataSorteio(new \DateTime(date('Y-m-d',$dataSorteio)));
        $lotofacil->setNumerosSorteados(array_slice($result, 3, 15));
        return $lotofacil;
    }

}