<?php

namespace Loteria\Adapter;
use Loteria\Adapter\Adapter;
/**
 * @author Fabiano
 */
class MegasenaAdapter extends Adapter{
    protected $urlBase='http://www1.caixa.gov.br/loterias/loterias/megasena/megasena_pesquisa_new.asp';
    protected $concurso;
    
    public function getConcurso() {
        return $this->concurso;
    }

    public function setConcurso($concurso) {
        $this->concurso = $concurso;
    }

    public function getSorteio() {
        if($this->getConcurso()){
            $url=$this->urlBase.'?opcao=concurso&submeteu=sim&txtConcurso='.$this->getConcurso();
        }else{
            $url=$this->urlBase;
        }
        $result = $this->exec($url);
       
        if ($result == '') {
            throw new \Loteria\Exception\ConcursoNotFoundException('MegasenaAdapter: Nenhum resultado encontrado.');
        }
        
        $result = explode('|', $result);
        if(count($result)<5){
            throw new \Loteria\Exception\ConcursoNotFoundException('MegasenaAdapter: Concurso '.$result[0].' não encontrado.');
        }
        $ms = new \Loteria\Entity\Megasena();
        $ms->setConcurso($result[0]);
        $data=  explode('/',$result[11]);
        $dataSorteio=  mktime(0, 0, 0, $data[1], $data[0], $data[2]);
        $ms->setDataSorteio(new \DateTime(date('Y-m-d',$dataSorteio)));
        $numeros= array_slice($result, 20, 1);
        $arr=explode(';',preg_replace("/\D/", ';',$numeros[0]));
        $resultado= array();
        foreach ($arr as $value) {
            if($value!==''){
                $resultado[]=$value;
            }
        }
        $ms->setNumerosSorteados($resultado);
        return $ms;
    }
    
}
