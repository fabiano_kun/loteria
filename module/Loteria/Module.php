<?php
namespace Loteria;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    public function getServiceConfig(){
        return array(
            'factories'=>array(
                'Loteria\Adapter\Lotofacil'=>function($sm){
                    return new \Loteria\Adapter\LotofacilAdapter();
                },
                'Loteria\Apurar\Lotofacil'=>function($sm){
                    return new \Loteria\Apuracao\Lotofacil\Lotofacil();
                },
                'Loteria\Adapter\Megasena'=>function($sm){
                    return new \Loteria\Adapter\MegasenaAdapter();
                },
                'Loteria\Apurar\Megasena'=>function($sm){
                    return new \Loteria\Apuracao\MegaSena\MegaSena();
                },
                'Loteria\Adapter\Quina'=>function($sm){
                    return new \Loteria\Adapter\QuinaAdapter();
                },
                'Loteria\Apurar\Quina'=>function($sm){
                    return new \Loteria\Apuracao\Quina\Quina();
                }
            )
        );
    }
}
