<?php
return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Conferencia\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'conf-lotofacil' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/lotofacil[/:action]',
                    'defaults' => array(
                        'controller'    => 'lotofacil',
                        'action'        => 'index',
                    ),
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    )
                )
            ),
            'conf-megasena' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/megasena[/:action]',
                    'defaults' => array(
                        'controller'    => 'megasena',
                        'action'        => 'index',
                    ),
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    )
                )
            ),
            'conf-quina' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/quina[/:action]',
                    'defaults' => array(
                        'controller'    => 'quina',
                        'action'        => 'index',
                    ),
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    )
                )
            ),
        ),
    ),
   
    'controllers' => array(
        'invokables' => array(
            'Conferencia\Controller\Index' => 'Conferencia\Controller\IndexController',            
            'lotofacil' => 'Conferencia\Controller\LotofacilController',
            'megasena' => 'Conferencia\Controller\MegasenaController',
            'quina' => 'Conferencia\Controller\QuinaController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'conferencia/index/index' => __DIR__ . '/../view/conferencia/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        )
    )
);
