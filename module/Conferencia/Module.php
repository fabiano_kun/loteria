<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Conferencia;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig(){
        return array(
            'factories'=>array(
                'Conferir\Lotofacil'=>function($sm){
                    return new \Conferencia\Service\LotofacilResultado($sm);
                },
                'Conferir\Megasena'=>function($sm){
                    return new \Conferencia\Service\MegasenaResultado($sm);
                },
                'Conferir\Quina'=>function($sm){
                    return new \Conferencia\Service\QuinaResultado($sm);
                }
            )
        );
    }
    /**
     * Register View Helper
     */
    public function getViewHelperConfig()
    {
        return array(
            # registrar View Helper com injecao de dependecia
            'factories' => array(
                'menuAtivo'  => function($sm) {
                    return new View\Helper\MenuAtivo($sm->getServiceLocator()->get('Request'));
                },
                'lotofacilHelper'=>function($sm){
                    return new View\Helper\Loteria\Lotofacil($sm->getServiceLocator()->get('Loteria\Adapter\Lotofacil'));
                },
                'megasenaHelper'=>function($sm){
                    return new View\Helper\Loteria\Megasena($sm->getServiceLocator()->get('Loteria\Adapter\Megasena'));
                },
                'quinaHelper'=>function($sm){
                    return new View\Helper\Loteria\Quina($sm->getServiceLocator()->get('Loteria\Adapter\Quina'));
                }
            )
        );
    }
}
