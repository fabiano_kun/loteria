<?php
namespace Conferencia\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Http\Request;
/**
 * @author Fabiano
 */
class QuinaController extends AbstractActionController {
    public function indexAction() {
        $this->getServiceLocator()
        ->get('viewhelpermanager')
        ->get('InlineScript')->appendFile('/js/conferir/quina.js');
        return new ViewModel();
    }
    public function conferirAction() {      
        return new JsonModel($this->execApuracao());
    }
    
    private function execApuracao(){
        $request= $this->request;
        $request instanceof Request;
        if($request->isPost()){
            $post=$request->getPost();
            if(!is_numeric($post['concurso-ini']) || !is_numeric($post['concurso-fim'])){
                return array('success'=>false,
                            'msg'=>'Os concursos devem ser numeros inteiros.');
            }
            if($post['concurso-fim']<$post['concurso-ini']){
                return array('success'=>false,
                            'msg'=>'O concurso inicial não deve ser maior que o concurso final.');
            }
            if($post['concurso-fim']-$post['concurso-ini'] > 30){
                return array('success'=>false,
                            'msg'=>'Favor não informar mais de 30 concursos de diferença');
            }
            set_time_limit(300);
            $lotas= $this->getServiceLocator()->get('Conferir\Quina');
            $numeros=$lotas->confereNumeros($post['concurso-ini'],$post['concurso-fim'],$post['numeros']);
            $dados=array();
            foreach ($numeros as $resultado){
                if($resultado instanceof \Loteria\Apuracao\Resultado ){
                    array_push($dados, array(
                        'concurso'=>$resultado->concurso,
                        'numerosSorteados'=>$resultado->numerosSorteados,
                        'numerosAcertados'=>$resultado->numerosAcertados,
                        'quantidadeAcertos'=>$resultado->quantidadeAcertos,
                        'mensagem'=>  ($resultado->mensagem)
                    ));
                }else{
                    $c=array_keys($resultado);
                    array_push($dados, array('concurso'=>$c[0],'numeros'=>'Sem sorteio para o concurso' ));
                }
            }            
            return array(
                 'success'=>true,
                 'msg'=>'Ok',
                 'dados'=>$dados
            );
        }else{
            return array(
                'success'=>false,
                'msg'=>'Não foi encotrado valores para conferencia.'
            );   
        }
    }
}
