<?php
namespace Conferencia\View\Helper\Loteria;

use Zend\View\Helper\AbstractHelper;
use Loteria\Adapter\MegasenaAdapter;
/**
 * @author Fabiano
 */
class Megasena extends AbstractHelper
{
    protected $adp;
 
    public function __construct(MegasenaAdapter $adapter)
    {
        $this->adp = $adapter;
    }
 
    public function __invoke()
    {
        $this->adp->setConcurso(null);
        $result=$this->adp->getSorteio();
        $html="<div class=''>";
        $html.="<label>Concurso:</label>&nbsp;".$result->getConcurso();        
        $html.="<br/><label>Data do sorteio:</label>&nbsp;".$result->getDataSorteio()->format('d/m/Y')."<br/>";
        $html.="<label>Números sorteados:</label><div class='text-center'>";
        $i=1;
        foreach ($result->getNumerosSorteados() as $n) {           
            $html.="<label class='btn btn-info' style='margin-bottom:4px;'>$n</label>&nbsp;";
        }
        $html.="</div></div>";
        return $html;
    }
}