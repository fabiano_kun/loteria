<?php
namespace Conferencia\View\Helper\Loteria;

use Zend\View\Helper\AbstractHelper;
use Loteria\Adapter\LotofacilAdapter;
/**
 * @author Fabiano
 */
class Lotofacil extends AbstractHelper
{
    protected $adp;
 
    public function __construct(LotofacilAdapter $adapter)
    {
        $this->adp = $adapter;
    }
 
    public function __invoke()
    {
        $this->adp->setConcurso(null);
        $result=$this->adp->getSorteio();
        $html="<div class=''>";
        $html.="<label>Concurso:</label>&nbsp;".$result->getConcurso();        
        $html.="<br/><label>Data do sorteio:</label>&nbsp;".$result->getDataSorteio()->format('d/m/Y')."<br/>";
        $html.="<label>Números sorteados:</label><div class='text-center'>";
        $i=1;
        foreach ($result->getNumerosSorteados() as $n) {
            $br=( $i%5==0)?'<br>':'';
            $html.="<label class='btn btn-info' style='margin-bottom:4px;'>$n</label>&nbsp;".$br;
            $i++;
        }
        $html.="</div></div>";
        return $html;
    }
}