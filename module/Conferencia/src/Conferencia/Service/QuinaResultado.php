<?php
namespace Conferencia\Service;
use Zend\Db\Adapter\Adapter;
use Loteria\Apuracao\Quina\Quina;
/**
 * @author Fabiano
 */
class QuinaResultado {
    /**
     *
     * @var \Zend\ServiceManager\ServiceManager; 
     */
    private $sm;
    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
        $this->sm=$sm;
    } 
    public function confereNumeros($concursoIni,$concursofim,array $numerosApostados){
        $numerossorteados=$this->getNumerosSorteados($concursoIni, $concursofim);
        $confLoteria=$this->sm->get('Loteria\Apurar\Quina');
        $confLoteria instanceof Quina;
        $confLoteria->adicionaNumerosApostados($numerosApostados);
        $result=array();
        foreach ($numerossorteados as $conc=>$concNumeros){
            if(is_array($concNumeros)){
                $confLoteria->adicionaNumerosSorteados($concNumeros);
                $resultado=end($confLoteria->apurarResultado()[0]);
                $resultado->concurso=$conc;
            }else{
                $resultado=array($conc=>$concNumeros);
            }
            array_push($result, $resultado);
        }
        return $result;
    }
    
    public function getNumerosSorteados($concursoIni,$concursofim){
        $concursos=array();
        for($c=$concursoIni;$c<=$concursofim;$c++){
            $concursos[]=(string)$c;
        }
        $numConcurso= $this->getNumerosBd($concursos);
        $concursoNot=array();
        
        foreach ($concursos as $concurso){
            if(!array_key_exists($concurso, $numConcurso)){
                $concursoNot[]=$concurso;
            }
        }
        foreach ($concursoNot as $conc) {
            $resp=$this->getNumersoSite($conc);
            if(!is_object($resp[$conc])){
                array_push($numConcurso, $resp[$conc]);    
            }else{
                $this->insereNumerosBd($conc, $resp[$conc]->getDataSorteio()->getTimestamp(), $resp[$conc]->getNumerosSorteados());
                $numConcurso[$conc]=$resp[$conc]->getNumerosSorteados();
            }
            
        }
        return $numConcurso;
        
    }
    
    private function getNumersoSite($concurso){
        $adMs=$this->sm->get('Loteria\Adapter\Quina');
        $adMs->setConcurso($concurso);
        try {
            $result=$adMs->getSorteio();
        } catch (\Loteria\Exception\ConcursoNotFoundException $exc) {
            return array($concurso=>'Concurso não encontrado');
        }
        return array($concurso=>$result);
        
    }
    
    private function getNumerosBd($concursos){
        
        $dbadapter=$this->sm->get('Zend\Db\Adapter');
        $dbadapter instanceof Adapter;
        $q="select tbl_concursoloteria.idconcurso,tbl_sorteioloteria.numerosorteado "
        . " from tbl_sorteioloteria inner join tbl_concursoloteria"
        . " on tbl_concursoloteria.idconcurso=tbl_sorteioloteria.idconcurso and "
        . " tbl_concursoloteria.idloteria=tbl_sorteioloteria.idloteria"
        . " where (tbl_concursoloteria.idconcurso in ('".implode("','", $concursos)."')) "
        . " and tbl_concursoloteria.idloteria=(select idloteria from tbl_loteria where tipoloteria='Quina' LIMIT 1) "
        . " order by tbl_concursoloteria.idconcurso";
        $result=$dbadapter->query($q);
        $ret=$result->execute();
        $dados=array();
        while($row=$ret->current()){
          $dados[$row['idconcurso']][]=$row['numerosorteado'];  
        }
        return $dados;
    }
    
    private function insereNumerosBd($concurso,$dataconcurso,array $numeros){
        $dbadapter=$this->sm->get('Zend\Db\Adapter');
        $dbadapter instanceof Adapter;
        $sb="select idloteria from tbl_loteria where tipoloteria='Quina' LIMIT 1";
        $loto=$dbadapter->query($sb);
        $rs=$loto->execute();        
        $row=$rs->current();
		if(!$row){
			$insloto="insert into tbl_loteria(tipoloteria) values('Quina');";
			$resultLoto=$dbadapter->query($insloto);
			$resultLoto->execute();  
			$loto=$dbadapter->query($sb);
			$rs=$loto->execute();        
			$row=$rs->current();
		}
        
        $insConc="insert into tbl_concursoloteria(idconcurso,idloteria,concurso,datasorteio) values($concurso,$row[idloteria],'$concurso',$dataconcurso);";
        $result=$dbadapter->query($insConc);
        $result->execute();        
        
        sort($numeros);
        foreach ($numeros as $numero) {
            $inssort="insert into tbl_sorteioloteria(idconcurso,idloteria,numerosorteado) values($concurso,$row[idloteria],$numero);";
            $r=$dbadapter->query($inssort);
            $r->execute();
        }
        
    }
}