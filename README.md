Loteria
=======================

Introdução
------------
Site com o intuito de aprender mais sobre ZF2
Atualmente o sistema faz a conferência de loterias como Lotofacil, Megasena e Quina.
A demostração do sistema está diponivel neste [link](https://loteria.facweb.com.br)

Banco de Dados
--------------
Utiliza banco de dados sqlite para guarda os números sorteados nas loterias.
