function Lotofacil(idForm){
    this.qntMinima=15;
    this.qntMaxima=18;
    this.acertoMinimo=11;
    this.form=$('#'+idForm);
    
    this.removeNumeroForm=function(numero){
        var el=this.form.find('input[value='+numero+']');
        el.remove();
    };    
    this.adicionaNumeroForm=function(numero){
        this.form.append('<input type="hidden" name="numeros[]" value="'+numero+'">');
    };
    this.podeConferir=function(){
        return (this.returnQntNumerosAdicionados()<=this.qntMaxima && this.returnQntNumerosAdicionados()>=this.qntMinima);
    };    
    this.podeAdicionar=function(){
        return this.returnQntNumerosAdicionados()<this.qntMaxima;
    };
    this.returnQntNumerosAdicionados=function(){
        return this.form.find('input[name^="numeros"]').length;
    };
    
}
var lotofacil= new Lotofacil('formnumeros');
$(function(){
    $('.resultado-loteria').css('display','none');
    $('.numeros').click(function(){
        if(!$(this).hasClass('btn-success')){        
            if(lotofacil.podeAdicionar()){
                $(this).addClass('btn-success').removeClass('btn-default');
                lotofacil.adicionaNumeroForm($(this).text());
            }else{
                alertMessage('Você pode selecionar no máximo '+lotofacil.qntMaxima);
            }
        }else{
            lotofacil.removeNumeroForm($(this).text());            
            $(this).addClass('btn-default').removeClass('btn-success');
        }
        $('#qntnumselecionado').html(lotofacil.returnQntNumerosAdicionados());

    });
    
    $('.conferir').click(function(){
        if($.trim($('#concurso-fim').val())==='' || $.trim($('#concurso-ini').val())==='' ){
            alertMessage('É necessário informar o início e termino do concurso.');
            return;
        }
        if(!lotofacil.podeConferir()){
            alertMessage('Você pode selecionar no mínimo '+lotofacil.qntMinima+' ou no máximo '+lotofacil.qntMaxima+' numeros.');
            return;
        }
        if(lotofacil.returnQntNumerosAdicionados()>lotofacil.qntMinima){
            var msg="Deseja realmente fazer a conferencia apostando "+lotofacil.returnQntNumerosAdicionados()+" numeros?";
            $('.confirm-message #message-confirm').html(msg);
            $('.confirm-message').modal('show');
        }else{
            submitForm();
        }
    });
    $('#confirmarenvio').click(function(){
        $('.confirm-message').modal('hide');
        submitForm();
    });
});

function alertMessage(msg){
    $('.alert-message #message-alert').html(msg);
    $('.alert-message').modal('show');
}
function submitForm(){
    $('.aguarde').toggle();
    $('.resultado-loteria').css('display','none');
    $.ajax({
        url: "lotofacil/conferir",
        dataType : 'json',
        data: $('#formnumeros').serializeArray(),
        type :'POST'
    }).done(function(dados) {
        mostrarResultado(dados);
    }).fail(function(){
        alertMessage('Aconteceu um problema ao conferir os numeros.<br> Tente novamente, caso o problema persista envie um email para fabiano.a.campos@gmail.com, obrigado!!')
        $('.aguarde').toggle();
    });
}
function mostrarResultado(result){
    var table=$('.resultado-loteria .table-resultados tbody');
    table.html("");
    if(!result.success){    
        alertMessage(result.msg);
        $('.aguarde').toggle();
        return;
    }
    
    var tableTotais=$('.resultado-loteria .table-totais tbody');
    tableTotais.html("");
    
    for(var idx in result.totais){
        var td="";        
            td+="<td>"+result.totais[idx].vezes+"</td>";
            td+="<td>"+result.totais[idx].quantidade+"</td>";
        tableTotais.append("<tr>"+td+"</tr>");
    }
    
    var css="";
    for(var idx in result.dados){
        var td="";        
        if(result.dados[idx].numeros== undefined){
            td="<td>"+result.dados[idx].concurso+"</td>";
            td+="<td>"+result.dados[idx].numerosSorteados+"</td>";
            td+="<td>"+result.dados[idx].numerosAcertados+"</td>";
            td+="<td class='text-center'>"+result.dados[idx].quantidadeAcertos+"</td>";
            td+="<td>"+result.dados[idx].mensagem+"</td>"; 
            css=result.dados[idx].quantidadeAcertos<lotofacil.acertoMinimo ?'':'class="success"';
        }else{
            td="<td><label>"+result.dados[idx].concurso+"</label></td>";
            td+="<td colspan='4'><label>"+result.dados[idx].numeros+"</label></td>";
            css="";
        }         
        table.append("<tr "+css+" >"+td+"</tr>");
    }
     $('.resultado-loteria').css('display','block');
    $('.aguarde').toggle();
    
}